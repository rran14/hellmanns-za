﻿/* Custom JS */
/* Analytics Track Event */
function trackAnalyticsEvent(name, detail, type){
    console.log("Inside method trackClickEvent !!!");
    if(UDM){
      UDM.evq.push(['trackEvent', type || 'Custom', name, detail]); 
    }
}
$(document).ready(function(){

    /* Twitter Code */
    try {
        getLocationDetails();
        $('.popup').click(function(event) {
            var width = 575,
                height = 400,
                left = ($(window).width() - width) / 2,
                top = ($(window).height() - height) / 2,
                url = this.href,
                opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;
            console.log(url);
            window.open(url, 'twitter', opts);
            return false;
        });
        var text = 'Check out the delicious burgers at ' + storeLocation.operatorname + ' @HellmannsZA #BurgerRouteZA NoPurNec 18+ Ends 11:59 p.m. on 30/06/2017';
        //var url = 'BurgerRouteContest';
        var url = window.location.href;
        var res = encodeURIComponent(url);
        var twitterurl = "https://twitter.com/share?text=" + encodeURIComponent(text) + "&url=" + res;
        $('.popup').attr('href', twitterurl);
    } catch (e) {}

/* Body Class */
var current_location = window.location.href.split('/'); 
var page = current_location[current_location.length - 1];

bodyclassvar = page.split('.html')[0];
if(bodyclassvar){
$("body").addClass(bodyclassvar.toLowerCase());
} else {
$("body").addClass('index');
}

/* POSTAL Code */
$('.btnSelectCity').click(function (e) {
	    e.preventDefault();
      var  selectedCityValue = $('#selectedProvince').html().trim();
	  var isCitySelected = $.inArray(selectedCityValue, storeLocator.cityList) > -1;
        if (!isCitySelected) {
          $('.selectCityError').show();
        }else {
			$('.selectCityError').hide();
			trackAnalyticsEvent("City", selectedCityValue.toUpperCase());
			var cityNameOnMapPage = getQSValue('selectedProvince');
			if(cityNameOnMapPage !== selectedCityValue){
				window.location = 'Maps.html?selectedProvince=' + selectedCityValue;
			}                   
			
	    }
	});
	$('#selectedProvince').on('keyup keypress', function (e) {
	    var keyCode = e.keyCode || e.which;
	    if (keyCode === 13) {
	        e.preventDefault();
	        $('.btnSelectCity').click();
	    }
	});	
	
    $("#home_pg_SweepsArea").find(".prize-btn > a").on("click", function(ev){
      ev.preventDefault();
      var prizeDetail = "Price Detail Page";
      trackAnalyticsEvent("Click", prizeDetail.toUpperCase());
      window.location = 'Prizes.html';
  });
 
  $("#home_pg_OlapicWidget").find(".view-gallery-btn > a").on("click", function(ev){
      ev.preventDefault();
      var galleryDetail = "Gallery Click";
      trackAnalyticsEvent("Click", galleryDetail.toUpperCase());
      window.location = 'Gallery.html';
  });
 
  $("#locationPg").find(".hasHttp > a").on("click", function(ev){
      ev.preventDefault();
      var locationDetail = $(this).attr("href");
      trackAnalyticsEvent("Location Link", locationDetail);
      window.open(locationDetail, "_blank");
  });
 
  $("#locationPg").find(".twitter a").on("click", function(ev){
      ev.preventDefault();
      var twitterDetail = "Header";
      trackAnalyticsEvent("Follow on Twitter", twitterDetail, "Referral");
  });
 
  $("#locationPg").find(".facebook a").on("click", function(ev){
      ev.preventDefault();
      var facebookDetail = "Header";
      trackAnalyticsEvent("Facebook", facebookDetail, "Referral");
  });
  
  // Select your City Script start
  $(".dropdown dt a").click(function(event) {
	   event.preventDefault();
		$(".dropdown dd ul").toggle();
	});
				
	$(".dropdown dd ul li").click(function(event) {		
		event.preventDefault();
	    var defaultValue = $('#city .defaultDDValue').html();
		var text = $(this).html();
		var isCitySelected = $.inArray(text, storeLocator.cityList) > -1;
		if(isCitySelected){
			$('.selectCityError').hide();
			
		}
		$(".dropdown dt a span").html(text);
		$(".dropdown dd ul").hide();
		// $("#result").html("Selected value is: " + getSelectedValue("sample"));
	});
				
	function getSelectedValue(id) {
		return $("#" + id).find("dt a span.value").html();
	}

	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (! $clicked.parents().hasClass("dropdown"))
			$(".dropdown dd ul").hide();
	});
	// Select your City Script end
	
	
});
/* OWL Carousel */

var storeLocator = {
	cityList : null,
	listItems : null,
	selector: "map",
	selectorCarousel: $("#sl-container"),
	map: null,
	stores : null,
	infowindow: null,
	city: null,
	currHub: null,
	mapHubs : new Array(),
	locations: null,
	iconColorDefault: "FF8940",
	iconColorCustom: "FF0000",
	iconColorCenter: "0000FF",
	mapZoom:11,
	isInitial : true,
	mapCenterLat:-33.92,
	mapCenterLng:151.25,
	closestStoresMax : 0,
	iconimg: ($(window).width() < 620) ? 29 : 40,
	iconimg1: ($(window).width() < 620) ? 48 : 67,
	storesLocal : new Array(),
	centerMarker: null
};
function owlCarouselInit(storeLength) {
    $('.owl-carousel').owlCarousel({
        autoHeight: true,
        loop: false,
        //margin: 20,
        responsiveClass: true,
        responsive: {
            320: {
                items: 1,
                nav: true
            },
            360: {
                items: 1,
                nav: true
            },
            480: {
                items: 1,
                nav: true
            },
            640: {
                items: 1,
                nav: true
            },
            767: {
                items: 1,
                nav: true,
                autoHeight: false
            },
            900: {
                items: 2,
                nav: true,
                autoHeight: false
            },
            1140: {
                items: 2,
                nav: true,
            }
        },
        onTranslated: function() {
            if ($("#owl-restaurant .owl-item:first").hasClass('active')) {
                $('.owl-controls .owl-next').css({
                    "opacity": "1",
                    "cursor": "pointer"
                });
                $('.owl-controls .owl-prev').css({
                    "opacity": "0.3",
                    "cursor": "default"
                });
            } else {
                $('.owl-controls .owl-prev').css({
                    "opacity": "1",
                    "cursor": "pointer"
                });
            }
            if ($("#owl-restaurant .owl-item:last").hasClass('active')) {
                $('.owl-controls .owl-prev').css({
                    "opacity": "1",
                    "cursor": "pointer"
                });
                $('.owl-controls .owl-next').css({
                    "opacity": "0.3",
                    "cursor": "default"
                });
            } else {
                $('.owl-controls .owl-next').css({
                    "opacity": "1",
                    "cursor": "pointer"
                });
            }
        }
    });
    //hide nav when store <= 6
    var storeLength = $(".sl-store-info").length;
    if (storeLength <= 6) {
        $('.owl-controls').hide();
    } else {
        $('.owl-controls').show();
    }

$ ('.owl-item:nth-child(even)').addClass('even');
}
/* Map Section */
(function(){
	$.ajax({
		type: "GET",
		url: "assets/js/restaurantdata.js",
		dataType: "json",
		async: false,
		crossDomain: false,
		success: function(data) {
		   storeLocator.listItems = data;
		   if(storeLocator.listItems.length > 0){
			   listGenerator(storeLocator.listItems);
		   }
		   if($('body').hasClass('location-page')) {
			singlemap();
		   }
		}
	});
}());
function listGenerator(storeData){
	storeLocator.cityList = new Array();
	var cityNameOnMapPage = getQSValue('selectedProvince');
	if(cityNameOnMapPage && cityNameOnMapPage != ''){
		$(".dropdown dt a span").html(cityNameOnMapPage);
	}
	$.each(storeData,function(index,value){
		storeLocator.cityList[index] = storeData[index].Province.trim();			
		   });
		   storeLocator.cityList = $.unique(storeLocator.cityList.sort()).sort();
	$.each(storeLocator.cityList,function(index,value){
		$('#city').append('<li>'+storeLocator.cityList[index]+'</li>');
	});
}
// Let's process the data from the data file
/* function getStoreData(){
	$.ajax({
		type: "GET",
		url: "assets/js/restaurantdata.js",
		dataType: "json",
		async: false,
		crossDomain: false,
		success: function(data) {
			storeLocator.listItems = data;
		   
		}
	});
} */
function mapInit(){
	 
	//storeLocator.listItems = data;
	storeLocator.stores = new Array();
	storeLocator.locations = new Array();
			
			
	 storeLocator.iconDefault = getIcon("•", storeLocator.iconDefault, storeLocator.iconColorDefault);
	 //storeLocator.iconCenter = getIcon("•", storeLocator.iconCenter, storeLocator.iconColorCenter);
			
	var bases = document.getElementsByTagName('base');
		storeLocator.city = getQSValue('selectedProvince');
	(bases.length > 0) ? storeLocator.stores.baseHref = bases[0].href : "";
	
	for (var i = 0; i < storeLocator.listItems.length; i++) {
		var store = {
			id: storeLocator.listItems[i].ID,
			address: storeLocator.listItems[i].Street,
			city: storeLocator.listItems[i].City,
            province : storeLocator.listItems[i].Province,
			country: storeLocator.listItems[i].Country,
			operator_Name: storeLocator.listItems[i].Operator_Name,
			locator_ID: storeLocator.listItems[i].Locator_ID,
			website: storeLocator.listItems[i].Website,
			location: new google.maps.LatLng(parseFloat(storeLocator.listItems[i].lat), parseFloat(storeLocator.listItems[i].lng)),
			distance:0
		}
		storeLocator.stores.push(store);
		//console.log(storeLocator.listItems[i].Operator_Name.replace("'", "-aeiou-"));
		try {
			 var querystring = '|Street=' + storeLocator.listItems[i].Street + '|city=' + storeLocator.listItems[i].City + '|Province=' + storeLocator.listItems[i].Province + '|Country=' + storeLocator.listItems[i].Country + '|OperatorName=' + (storeLocator.listItems[i].Operator_Name).replace("'", "-aeiou-") + '|website=' + storeLocator.listItems[i].Website + '|lat=' + storeLocator.listItems[i].lat + '|lng=' + storeLocator.listItems[i].lng + '|locatorid=' + storeLocator.listItems[i].Locator_ID + '|ID=' + storeLocator.listItems[i].ID,
			 loc = ['<a href="javascript:SetData(\'' + querystring.replace("'", "-aeiou-") + '\',' + storeLocator.listItems[i].ID + ',\'' + storeLocator.stores.baseHref + '\')"><b>' + storeLocator.listItems[i].Operator_Name + '</b> ' + storeLocator.listItems[i].Street + '<em>' + storeLocator.listItems[i].City + ', ' + storeLocator.listItems[i].Province + ' ' + storeLocator.listItems[i].Country + '</em></a>', storeLocator.listItems[i].lat, storeLocator.listItems[i].lng, storeLocator.listItems[i].ID, storeLocator.listItems[i]]; 
			
			storeLocator.locations.push(loc);
			
		} catch (e) {
			//console.log(e);
		}

	}
	var centerPoint = new google.maps.LatLng(storeLocator.listItems[0].lat, storeLocator.listItems[0].lng);
	var mapOptions = {
		zoom: storeLocator.mapZoom,
		//minZoom: StoreLocator.mapZoomMin,
		center: centerPoint,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		draggable: true //!Utils.isMobile()
	}
			
	storeLocator.map = new google.maps.Map(document.getElementById(storeLocator.selector), mapOptions);

	storeLocator.infowindow = new google.maps.InfoWindow();
 	
	
	

	if (storeLocator.city != null && storeLocator.city != undefined && storeLocator.city != '') {
		google.maps.event.addListener(storeLocator.map, 'bounds_changed', function() {
			if(storeLocator.isInitial) {
				getLocationMarker();
				centerOnHub();
				storeLocator.isInitial = false;
			}
        });

} else {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: parseFloat(position.coords.latitude),
                lng: parseFloat(position.coords.longitude)
            };

			updateMap(new google.maps.LatLng(pos.lat, pos.lng));
	
        }, function() {
            storeLocator.infowindow = new google.maps.InfoWindow(storeLocator.map);
            handleLocationError(true, storeLocator.infowindow, storeLocator.map.getCenter());
        });
    } else {
        storeLocator.infowindow = new google.maps.InfoWindow(storeLocator.map);
        // Browser doesn't support Geolocation
        handleLocationError(false, storeLocator.infowindow, storeLocator.map.getCenter());
    }
}
	
google.maps.event.addListener(storeLocator.map, 'dragend', function () {
		updateMap();
	});

}
function centerOnHub(page) {
	var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ address: storeLocator.city }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                   // callback(results[0].geometry.location);
				    storeLocator.infowindow.setPosition(results[0].geometry.location);
					storeLocator.map.setCenter(results[0].geometry.location);
					 storeLocator.centerMarker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: storeLocator.map,
						icon: storeLocator.iconCenter,
						zIndex: 9999
					}); 
					
					  var center = storeLocator.map.getCenter();
					for (var i = 0; i < storeLocator.stores.length; i++) {
						storeLocator.stores[i].distance = google.maps.geometry.spherical.computeDistanceBetween(center, storeLocator.stores[i].location);
					}
					var hub = sortObjectsBy(storeLocator.stores, "distance", true)[0];
				
					storeLocator.currHub = hub;
					updateMap(storeLocator.currHub.location);  
                }
            });
		
	
}
function updateMap(newCenter) {
	 if (newCenter) {
			storeLocator.map.setZoom(storeLocator.mapZoom);
			storeLocator.map.panTo(newCenter);
		}

	getClosestStores(storeLocator.closestStoresMax);
	storeLocator.centerMarker.setPosition(storeLocator.map.getCenter());
}
function getClosestStores(count) {
	hideLocalStores();
	storeLocator.storesLocal= new Array();
	var bounds = storeLocator.map.getBounds();
		var center = storeLocator.map.getCenter();
		for (var i = 0; i < storeLocator.stores.length; i++) {
			if (storeLocator.city.toUpperCase() === storeLocator.stores[i].city.toUpperCase()) {
				storeLocator.stores[i].distance = google.maps.geometry.spherical.computeDistanceBetween(center, storeLocator.stores[i].location);
				storeLocator.storesLocal.push(storeLocator.stores[i]);
			}
		}

		storeLocator.storesLocal = sortObjectsBy(storeLocator.storesLocal, "distance", true);
		showLocalStores();
		displayClosestStores();
	
    
}
function hideLocalStores() {
	if (storeLocator.storesLocal != null) {
		for (var i = 0; i < storeLocator.storesLocal.length; i++) {
			if(storeLocator.storesLocal[i].marker) {
			  storeLocator.storesLocal[i].marker.setMap(null);
			}
		}
	}
}
function showLocalStores() {
	for (var i = 0; i < storeLocator.storesLocal.length; i++) {
		var icon = (i < storeLocator.closestStoresMax) ? getIcon(i + 1, storeLocator.iconClose, storeLocator.iconColorClose) : storeLocator.iconDefault;
		storeLocator.storesLocal[i].marker = new google.maps.Marker({
			position: storeLocator.storesLocal[i].location,
			map: storeLocator.map,
			icon: icon,
			title: storeLocator.storesLocal[i].name,
			
		});

		addMarkerListener(storeLocator.storesLocal[i].marker, i, i);
	}
}
function getIcon(label, options, color) {
	var icon = {};
	if (options == null) {
		icon = {
			url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + label + "|" + color,
			size: null,
			origin: null,
			anchor: new google.maps.Point(12, 18)
		};
	} else {
		$.extend(icon, options);
		icon.size = null;
		icon.origin = null;
		if (icon.prefix != null && icon.suffix != null) {
			icon.url = icon.prefix + label + icon.suffix;
		}
		if (Array.isArray(icon.anchor)) {
			icon.anchor = new google.maps.Point(icon.anchor[0], icon.anchor[1]);
		}
	}
	return icon;
}
/* Get the value of a querystring parameter */
function getQSValue(param) {
	var qs = location.search.substring(1).split("&");
	for (var i = 0; i < qs.length; i++) {
		var pair = qs[i].split("=");
		if (pair[0] == param) {
			return decodeURIComponent(pair[1]);
		}
	}
	return null;
}
function addMarkerListener(marker, index) {
	google.maps.event.addListener(marker, 'click', (function(marker, index) {
        return function() {
          var restaurantDetail = storeLocator.storesLocal[index];
		  
		  storeLocator.infowindow.setContent(getStoreInfoToMap(storeLocator.storesLocal[index]));
		  storeLocator.infowindow.open(storeLocator.map, marker);
		
        }
      })(marker, index));
} 
function getStoreInfoToMap (store) {
     var info = '';
     
    	info +='<a href="' + location.protocol + '//' + location.hostname + location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1) + 'location.html?id=' + store.id + '" onclick="UDM.evq.push([\'trackEvent\', \'Other\', \'Widgets\', \'Restauranted Selected  -' + store.operator_Name + '\']);"><b>'+store.operator_Name +'</b>' +store.address +'<em>'+store.city +', '+ store.province +' ' +store.country +'</em></a>',store.lat,store.lng, store.locator_ID,store;
    
    return info;
}
/* Sort an array using a sort function or by value in ascending or descending order */
function sortObjectsBy(array, compare, ascendingOrder) {
	var useCompareFunc = (typeof compare == "function");
	for (var i = 0; i < (array.length - 1); i++) {
		for (var j = i + 1; j < array.length; j++) {
			var swap = false;
			if (useCompareFunc) {
				swap = compare(array[i], array[j], ascendingOrder);
			} else {
				if (ascendingOrder) {
					swap = array[i][compare] > array[j][compare];
				} else {
					swap = array[i][compare] < array[j][compare];
				}
			}
			if (swap) {
				var dummy = array[i];
				array[i] = array[j];
				array[j] = dummy;
			}
		}
	}
	return array;
}
function getnewCenter(city, callback) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        address: city
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            callback(results[0].geometry.location);
        }
    });
}
 
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}


function getStoreInfo(store) {

    var info = '<div class="sl-store-info">';


    var querystring = '|Street=' + store.address  + '|city=' + store.city + '|province=' + store.province + '|Country=' + store.country + '|OperatorName=' + (store.operator_Name).replace("'", "-aeiou-") + '|website=' + store.website + '|lat=' + store.lat + '|lng=' + store.lng + '|locatorid=' + store.locator_ID + '|ID=' + store.id;

    info += '<div class="imgLocation"><a href="' + location.protocol + '//' + location.hostname + location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1) + 'location.html?id=' + store.id + '" ><img src="' + getImageName(store.locator_ID, storeLocator.stores.baseHref) + '" onerror="this.src=\'assets/img/burger5.jpg\';" alt="" "/></a></div>'
    info += '<div class="textItem">';

    info += '<a href="' + location.protocol + '//' + location.hostname + location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1) + 'location.html?id=' + store.id + '" ><div class="sl-store-name">' + store.operator_Name + '</div>';
    info += '<h4>LOCATION</h4><div class="sl-store-address"> ' + store.address  + '</div>';
    info += '<div class="sl-store-cityStateZip">' + store.city + ', ' + store.province + ' </div>';
    info += '</a></div>';
    info += '<div class="clear"></div>';
    info += '</div>';
    return info;
}
/* Get the current page's name without file extension */
function getPageName(indexPageName) {
	var pageName = location.pathname.split("/");
	pageName = pageName[pageName.length - 1].split(".")[0].toLowerCase();
	if (pageName == "") {
		pageName = (indexPageName) ? indexPageName.toLowerCase() : "default";
	}
	return pageName;
}
/* Get position index within array for matching value or object property value */
function getArrayIndex(array, search, propName) {
	for (var i = 0; i < array.length; i++) {
		if (propName != null) {
			if (array[i][propName] == search) {
				return i;
			}
		} else if (array[i] == search) {
			return i;
		}
	}
	return -1;
}

function getFromArray(array, search, propName) {
	var index = getArrayIndex(array, search, propName);
	if (index != -1) {
		return array[index];
	}
	return null;
}
function displayClosestStores() {
	  var TotalRow = 0;
            var IsEnd = 3;
            var test = '';
            storeLocator.selectorCarousel.find('.sl-closest-stores').html("<ol id='owl-restaurant' class='owl-carousel owl-theme'></ol>");
            for (var i = 0; i < storeLocator.storesLocal.length; i++) {
                if (TotalRow == 0) {
                    test += "<div class='item-col'>" + getStoreInfo(storeLocator.storesLocal[i]);
                    TotalRow++;
                } else {
                    test += getStoreInfo(storeLocator.storesLocal[i]);
                    TotalRow++;
                }
                if (TotalRow == IsEnd) {
                    test += "</div>";
                    TotalRow = 0
                }
            }
            var temp = test;
            if (storeLocator.storesLocal.length % 3 != 0) {
                temp += "</div>";
            }
            storeLocator.selectorCarousel.find('.sl-closest-stores ol').append(temp);
            owlCarouselInit(storeLocator.storesLocal.length);
}
function getLocationMarker(pos) {
    google.maps.event.addListener(storeLocator.infowindow, 'domready', function() {
        var iwOuter = $('.gm-style-iw');
        var iwBackground = iwOuter.prev();
        iwBackground.children(':nth-child(1)').css({
            'display': 'none'
        });
        iwBackground.children(':nth-child(2)').css({
            'display': 'none'
        });
        iwBackground.children(':nth-child(4)').css({
            'display': 'none'
        });
        if ($(window).width() < 620) {
            iwBackground.children(':nth-child(3)').attr('style', function(i, s) {
                return s + 'margin-left:-15px;z-index: 9;margin-top: 0px;'
            });
        } else {
            iwBackground.children(':nth-child(3)').attr('style', function(i, s) {
                return s + 'margin-left:-15px;z-index: 9;margin-top: -3px;'
            });
        }
        iwBackground.children(':nth-child(3)').children(':nth-child(1)').attr('style', function(i, s) {
            return s + 'width: 30px !important;height:35px !important;'
        });
        iwBackground.children(':nth-child(3)').children(':nth-child(1)').children(':nth-child(1)').attr('style', function(i, s) {
            return s + 'left: 0 !important; width:54px !important;transform: skewX(50deg) !important;'
        });
        iwBackground.children(':nth-child(3)').children(':nth-child(2)').attr('style', function(i, s) {
            return s + 'width: 30px !important;height:35px !important;left: 22px !important;'
        });
        iwBackground.children(':nth-child(3)').children(':nth-child(2)').children(':nth-child(1)').attr('style', function(i, s) {
            return s + 'right: 0 !important;left:auto !important;width:54px !important;transform: skewX(-50deg) !important;'
        });
    });
   }

function setDetailModel() {
    var zipCode = getQSValue('zipCode');
    ReadDataCache(function(data) {
        
            for (var i = 0; i < storeLocator.listItems.length; i++) {
                var id = getQSValue('id');
                if (storeLocator.listItems[i].ID == id) {

                    storeLocation = {
                        street: storeLocator.listItems[i].Street,
                        city: storeLocator.listItems[i].City,
                        province: storeLocator.listItems[i].Province,
                        country: storeLocator.listItems[i].Country,
                        operatorname: storeLocator.listItems[i].Operator_Name,
                        website: storeLocator.listItems[i].Website,
                        lat: storeLocator.listItems[i].lat,
                        lng: storeLocator.listItems[i].lng,
                        locatorid: storeLocator.listItems[i].Locator_ID,
                        id: getQSValue('id')
                    };

                    return;
                }
            }
    });

}

function getLocationDetails() {
    setDetailModel();

    var details = ' <h3>' + storeLocation.operatorname + '</h3><p> ' + storeLocation.street + ' ' + '<br/>' + storeLocation.city + ', ' + storeLocation.province + ' ' + storeLocation.country + '<br><span class="hasHttp"><a target="_blank" href="' + storeLocation.website+ '">' + storeLocation.website + '</a></span></p>';
    $('.location-top-row-col-left-sortinfo').append(details);
    var src = getImageName(storeLocation.locatorid, baseHref);
    $('.product_img').append('<img alt="" id="restaurant-img" src="' + src + '" onerror="this.src=\'assets/img/burger5.jpg\';" >');

}

function getDirectionsUrl() {
    if (storeLocation == undefined) {
        setDetailModel();
    }
    window.open("https://maps.google.com/maps/dir/Current+Location/" + encodeURIComponent(storeLocation.operatorname + '+' + storeLocation.street + '+' + storeLocation.city + '+' + storeLocation.province + '+' + storeLocation.zip + '+' + storeLocation.country), "_blank");
}

/* Location Details Page */

function singlemap() {
    var locations = [];
    if (storeLocation == undefined) {
        setDetailModel();
    }

 locations = [
      ['<span class="text-here"><b>'+storeLocation.operatorname +'</b>'+storeLocation.street +'<em>'+storeLocation.city +', '+ storeLocation.province +' ' +storeLocation.country +'</em></span>', storeLocation.lat, storeLocation.lng]
    ];
   /*  if ($(window).width() < 620) {
        var iconimg = 25;
        var iconimg1 = 44;
    } else {
        var iconimg = 33;
        var iconimg1 = 60;
    } */
    var pinIcon = new google.maps.MarkerImage(
        "assets/img/map-icon-details.png",
        null, /* size is determined at runtime */
        null, /* origin is 0,0 */
        null, /* anchor is bottom center of the scaled image */
        new google.maps.Size(storeLocator.iconimg, storeLocator.iconimg1)
    );
    storeLocator.map = new google.maps.Map(document.getElementById('single-map'), {
        zoom: 10,
        center: new google.maps.LatLng(-33.92, 151.50),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: storeLocator.map,
            icon: storeLocator.iconDefault
        });
        infowindow.setContent(locations[i][0]);
        infowindow.open(storeLocator.map, marker);
    }

    google.maps.event.addListener(infowindow, 'domready', function() {
        var iwOuter = $('.gm-style-iw');
        var iwBackground = iwOuter.prev();
        iwBackground.children(':nth-child(1)').css({
            'display': 'none'
        });
        iwBackground.children(':nth-child(2)').css({
            'display': 'none'
        });
        iwBackground.children(':nth-child(4)').css({
            'display': 'none'
        });
        if ($(window).width() < 620) {
            iwBackground.children(':nth-child(3)').attr('style', function(i, s) {
                return s + 'display: none !important;'
            });
        } else {
            iwBackground.children(':nth-child(3)').attr('style', function(i, s) {
                return s + 'display: none !important;'
            });
        }
        iwBackground.children(':nth-child(3)').children(':nth-child(1)').attr('style', function(i, s) {
            return s + 'display: none !important;'
        });
        iwBackground.children(':nth-child(3)').children(':nth-child(1)').children(':nth-child(1)').attr('style', function(i, s) {
            return s + 'display: none !important;'
        });
        iwBackground.children(':nth-child(3)').children(':nth-child(2)').attr('style', function(i, s) {
            return s + 'display: none !important;'
        });
        iwBackground.children(':nth-child(3)').children(':nth-child(2)').children(':nth-child(1)').attr('style', function(i, s) {
            return s + 'display: none !important;'
        });
    });
}

function setMetaTags(operatorname) {

    return "Check out the delicious burgers and " + operatorname + " on the Hellmann's® Burger Route! Discover & share burgers for your chance to win! No purchase necessary. Must be at least the age of majority in your jurisdiction. Ends 11:59 p.m. on 30/06/2017. For rules, visit  http://www.hellmanns.co.za/article/detail/1312229/Burger-Route-Terms-and-Conditions";

}
/* if($('body').hasClass('maps')) {
google.maps.event.addDomListener(window, "load", getStoreData);
} */
 $(document).ready(function () {
	// getStoreData();
	 if($('body').hasClass('maps')) {
		google.maps.event.addDomListener(window, "load", mapInit);
	 }
});

 